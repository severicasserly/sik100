# SIK100 Webpage

## Infrastructure

Infrastructure defined [here](https://gitlab.com/sahkoinsinoorikilta/vtmk/infra).

### Production

- [https://sik100.fi](https://sik100.fi)
- [https://api.sik100.fi](https://api.sik100.fi)

### Dev

- [https://dev.sik100.fi](https://dev.sik100.fi)
- [https://api.dev.sik100.fi](https://api.dev.sik100.fi)

## Frontend

- [Next JS](https://nextjs.org/)
- [Typescript](https://www.typescriptlang.org/)
- [styled-components](https://styled-components.com/)

### Prerequisites

- Node v12
- ```cd frontend && npm install```
- Copy `.env.sample` as `.env.local`, and fill in missing environment variables

### Commands

- ```npm run start```: Start development server on localhost:3000
- ```npm run build```: Build frontend locally
- ```npm run start-prod```: Start built server on localhost:80
- ```npm run gen-sitemap```: Generates sitemap.xml
- ```npm run lint```: Runs linters in parallel; ESLint for TS, Stylelint for CSS (styled-components)

Linters are run as git hook before push.

## Backend

- [Express](https://expressjs.com/)
- [Typescript](https://www.typescriptlang.org/)
- Integrates with [SIK Web2.0](https://gitlab.com/sahkoinsinoorikilta/vtmk/web2.0-backend)

### Prerequisites

- Node v12
- ```cd backend && npm install```
- Copy `.env.sample` as `.env`, and fill in missing environment variables

### Commands

- ```npm run start```: Runs server in development mode (automatic refresh on save)
- ```npm run build```: Compile TS files into JS
- ```npm run start-prod```: Start server from complied JS in /dist
- ```npm run lint```: ESLint
