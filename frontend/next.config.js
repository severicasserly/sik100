const withPWA = require("next-pwa");

const localeSubpaths = {};

const settings = {
  target: "serverless",
  basePath: "",
  experimental: {
    jsconfigPaths: true,
  },
  publicRuntimeConfig: {
    localeSubpaths,
  },
  pwa: {
    disable: process.env.NODE_ENV === "development",
    register: true,
    dest: "public",
  },
  images: {
    domains: [
      "sahkoinsinoorikilta.fi",
      "api.sahkoinsinoorikilta.fi",
      "static.sahkoinsinoorikilta.fi",
      "prod.sahkoinsinoorikilta.fi",
      "dev.sahkoinsinoorikilta.fi",
      "api.dev.sahkoinsinoorikilta.fi",
    ],
  },
};

module.exports = withPWA(settings);
