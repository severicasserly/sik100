import React from "react";
import styled from "styled-components";

import { useTranslation } from "../../i18n";
import contactsJson from "./contacts.json";

import ContactImage from "../../components/ContactImage";
import Footer from "../../components/Footer";
import Header from "../../components/Header";

const getImageFromEmail = (email: string): string => {
  const name = email.split("@")[0].toLowerCase();
  if (name) {
    return `/images/${name}.jpg`;
  }
  return "";
};

const contacts = contactsJson.map((c) => (
  {
    ...c,
    image: getImageFromEmail(c.email),
  }
));

const ContactsContainer = styled.div`
  display: flex;
  flex-flow: row wrap;
  justify-content: space-evenly;
`;

const EmailLink = styled.a`
  color: ${(p) => p.theme.colors.white};
`;

export const ContactPage: React.FC = () => {
  const { t } = useTranslation();
  return (
    <>
      <Header title={t("contactspage")}>
        <p>
          {t("contactBannerP1")}
        </p>
        <p>
          {t("30 henkistä toimikuntaa johtaa tältä sivulta löytyvät päävastuuhenkilöt")}
          .
        </p>
        <p>
          {t("Toimikunnan tavoitat helpoiten lähettämällä sähköpostia osoitteeseen")}
          &nbsp;
          <EmailLink href="mailto:contact@sik100.fi">
            contact@sik100.fi
          </EmailLink>
          .
        </p>
      </Header>
      <main>
        <section>
          <ContactsContainer>
            {contacts.map(
              ({
                name,
                image,
                role,
                email,
              }) => <ContactImage key={name} name={name} image={image} roleName={role} email={email} />,
            )}
          </ContactsContainer>
        </section>
      </main>
      <Footer />
    </>
  );
};

export default ContactPage;
