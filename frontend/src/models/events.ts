/* eslint-disable camelcase */

export type EventTag = {
  id: number;
  slug: string;
  name: string;
  name_fi: string;
  name_en: string;
  icon: string;
}

export type EventSignupForm = {
  id: number;
  title: string;
  isOpen: boolean;
  start_time: string;
  end_time: string;
  questions: any[];
  schema: any;
}

export type Event = {
  id: number;
  tag_id: number[];
  tags: EventTag[];
  visible: boolean;
  title_fi: string;
  title_en: string;
  description_fi: string;
  description_en: string;
  content_fi: string;
  content_en: string;
  start_time: string;
  end_time: string;
  location_fi: string;
  location_en: string;
  signup_id: number[];
  signupForm: EventSignupForm[];
  image?: string;
}
