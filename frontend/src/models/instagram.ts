type MediaTypes = "IMAGE" | "CAROUSEL_ALBUM" | "VIDEO";

export type InstagramObject = {
  img: string;
  link: string;
  text: string;
  id: string;
  type: MediaTypes;
}
