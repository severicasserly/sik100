import Axios from "axios";
import useSWR from "swr";
import config from "../config";
import { Event } from "../models/events";

const API_URL = "/events";

export const fetchData = async (): Promise<Event[]> => {
  if (config.backendUrl) {
    const url = `${config.backendUrl}${API_URL}`;
    const result = await Axios.get(url);
    return result.data;
  }
  throw new Error("No required runtime environment variables present!");
};

const useFetchEvents = (initialData?: Event[]) => {
  const { data, error } = useSWR<Event[] | undefined>(API_URL, fetchData, { initialData });
  return {
    events: data,
    error,
  };
};

export default useFetchEvents;
