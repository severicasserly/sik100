import Axios from "axios";
import useSWR from "swr";
import config from "../config";
import { InstagramObject } from "../models/instagram";

const API_URL = "/instagram";

export const fetchData = async (): Promise<InstagramObject[]> => {
  if (config.backendUrl && config.instagramImageCount) {
    const url = `${config.backendUrl}${API_URL}?count=${config.instagramImageCount}`;
    const result = await Axios.get(url);
    return result.data.data;
  }
  throw new Error("No required runtime environment variables present!");
};

const useFetchInstagram = (initialData: InstagramObject[]) => {
  const { data, error } = useSWR<InstagramObject[]>(API_URL, fetchData, { initialData });
  return {
    instagramFeed: data,
    error,
  };
};

export default useFetchInstagram;
