import NextI18Next from "next-i18next";

import Config from "next/config";
import path from "path";

const NextI18NextInstance = new NextI18Next({
  defaultLanguage: "fi",
  defaultNS: "common",
  localeSubpaths: Config().publicRuntimeConfig.localeSubpaths,
  localePath: path.resolve("./public/locales"),
  otherLanguages: ["en"],
});

export const { appWithTranslation, useTranslation } = NextI18NextInstance;

export default NextI18NextInstance;
