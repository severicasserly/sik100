import React, { useState, useEffect, useRef } from "react";
import Axios from "axios";
import { toast } from "react-toastify";
import ReactMarkdown from "react-markdown/with-html";
import styled from "styled-components";
import Image from "next/image";
import { useTranslation } from "../../i18n";
import Button from "../Button";
import SignupForm from "../SignupForm/SignupForm";
import { EventSignupForm } from "../../models/events";
import config from "../../config";
import { isBrowser } from "../../utils";

type EventCardProps = {
  title: string;
  start: string;
  location: string;
  content: string;
  signupForm?: EventSignupForm;
  focused?: boolean;
  image?: string;
}

const EventContainer = styled.article`
  display: flex;
  flex-direction: column;
  background-color: ${(p) => p.theme.colors.blue3};
  margin: ${(p) => p.theme.spacing.sm} 0;

  & > div:first-of-type {
    display: flex;
    flex-flow: row wrap;
    cursor: pointer;
  }
`;

const Date = styled.p`
  margin: 0;
  min-width: 90px;
  flex: 1 0;
  background: ${(p) => p.theme.colors.goldGradient};
  color: ${(p) => p.theme.colors.darkBlue1};
  display: flex;
  justify-content: center;
  align-items: center;
  font-size: ${(p) => p.theme.fontSizes.biggest};
  font-weight: bold;
  letter-spacing: 2px;

  @media screen and (max-width: ${(p) => p.theme.breakpoints.sm}) {
    font-size: ${(p) => p.theme.fontSizes.bigger};
  }

  @media screen and (max-width: ${(p) => p.theme.breakpoints.xxs}) {
    font-size: ${(p) => p.theme.fontSizes.h2};
  }

  @media screen and (max-width: ${(p) => p.theme.breakpoints.xxxs}) {
    font-size: ${(p) => p.theme.fontSizes.normal};
  }
`;

const TextContainer = styled.div`
  flex: 4 0;
  white-space: nowrap;
  text-overflow: ellipsis;
  overflow: hidden;
  margin: ${(p) => `${p.theme.spacing.sm} ${p.theme.spacing.md}`};

  & > p {
    white-space: nowrap;
    text-overflow: ellipsis;
    overflow: hidden;
    margin: ${(p) => p.theme.spacing.xxs} 0;
    color: ${(p) => p.theme.colors.white};

    @media screen and (max-width: ${(p) => p.theme.breakpoints.xxxs}) {
      font-size: ${(p) => p.theme.fontSizes.small};
    }
  }

  @media screen and (max-width: ${(p) => p.theme.breakpoints.sm}) {
    flex: 3 0;
  }

  @media screen and (max-width: ${(p) => p.theme.breakpoints.xs}) {
    margin-right: ${(p) => p.theme.spacing.xxs};
  }
`;

const Name = styled.p`
  color: ${(p) => p.theme.colors.turquoise} !important;
  font-size: ${(p) => p.theme.fontSizes.bigger};
  text-transform: uppercase;
  font-weight: 700;
  letter-spacing: 1px;

  @media screen and (max-width: ${(p) => p.theme.breakpoints.sm}) {
    font-size: ${(p) => p.theme.fontSizes.h2};
  }

  @media screen and (max-width: ${(p) => p.theme.breakpoints.xxs}) {
    font-size: ${(p) => p.theme.fontSizes.normal};
  }
`;

const OpenableContent = styled.div`
  color: ${(p) => p.theme.colors.white};
  background-color: ${(p) => p.theme.colors.blue6};
  padding: ${(p) => `${p.theme.spacing.lg} ${p.theme.spacing.xlg}`};

  @media screen and (max-width: ${(p) => p.theme.breakpoints.md}) {
    padding: ${(p) => `${p.theme.spacing.lg}`};
  }

  @media screen and (max-width: ${(p) => p.theme.breakpoints.sm}) {
    padding: ${(p) => `${p.theme.spacing.md}`};
  }

  @media screen and (max-width: ${(p) => p.theme.breakpoints.xxs}) {
    padding: ${(p) => `${p.theme.spacing.sm} ${p.theme.spacing.md}`};
  }

  & > p {
    margin: 12px 0;
  }

  & > form {
    margin-top: ${(p) => p.theme.spacing.sm};
  }

  & > h3 {
    color: ${(p) => p.theme.colors.secondaryGold};
  }

  a {
    font-weight: bold;
    color: ${(p) => p.theme.colors.secondaryGold};
    letter-spacing: 1px;
    text-decoration: underline;

    &:hover {
      color: ${(p) => p.theme.colors.primaryGold};
    }
  }

  & > table {
    tr {
      vertical-align: top;

      td {
        padding: ${(p) => p.theme.spacing.xxs} ${(p) => p.theme.spacing.xs};
        word-break: break-word;
      }

      td:first-of-type {
        word-break: unset;
      }
    }
  }

  button {
    margin-top: ${(p) => p.theme.spacing.sm};
  }
`;

const ExpandableIcon = styled.div`
  font-size: 32px;
  height: 48px;
  width: 48px;
  text-align: center;
  justify-self: center;
  align-self: center;
  margin-right: 8px;
  color: ${(p) => p.theme.colors.white};

  @media screen and (max-width: ${(p) => p.theme.breakpoints.sm}) {
    height: 32px;
    width: 32px;
    font-size: 24px;
  }
`;

// eslint-disable-next-line camelcase
const sendSignup = async (signupForm_id: number, answer: string) => {
  const url = `${config.backendUrl}/signup`;
  const result = await Axios.post(url, {
    signupForm_id,
    answer,
  });
  return result.data.data;
};

const EventCard: React.FC<EventCardProps> = ({
  title, start, location, content, signupForm, focused, image,
}) => {
  const [isOpen, setOpen] = useState(focused);
  const [showSignup, setSignupShown] = useState(focused);
  const htmlElRef = useRef<HTMLElement>(null);
  const { t } = useTranslation();

  useEffect(() => {
    if (focused && isBrowser()) {
      window.scrollTo(0, htmlElRef.current?.offsetTop ?? 0);
    }
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const eventInfo = (
    <>
      {image && (
      <Image
        src={image}
        alt={title}
        layout="responsive"
        width={200}
        height={100}
        objectFit="scale-down"
        loading="lazy"
      />
      )}
      <ReactMarkdown escapeHtml={false}>
        {content}
      </ReactMarkdown>
      {signupForm && (
        <Button onClick={() => setSignupShown(true)}>
          {t("Ilmoittaudu")}
        </Button>
      )}
    </>
  );

  const signupInfo = signupForm && (
    <>
      <Button onClick={() => setSignupShown(false)}>
        {t("Peruuta")}
      </Button>
      <SignupForm
        signup={signupForm}
        onSubmit={async (data) => {
          try {
            const resp = await sendSignup(signupForm.id, data.formData);
            toast.success("Sign-up submitted successfully", {
              position: "bottom-right",
              autoClose: 10000,
              hideProgressBar: false,
              closeOnClick: true,
              pauseOnHover: true,
              draggable: true,
              progress: undefined,
            });
            setSignupShown(false);
          } catch (error) {
            console.error(error);
            toast.error("Bad request", {
              position: "bottom-right",
              autoClose: 10000,
              hideProgressBar: false,
              closeOnClick: true,
              pauseOnHover: true,
              draggable: true,
              progress: undefined,
            });
          }
        }}
      />
    </>
  );

  return (
    <EventContainer ref={htmlElRef}>
      {/* eslint-disable jsx-a11y/click-events-have-key-events */}
      {/* eslint-disable jsx-a11y/no-static-element-interactions */}
      <div onClick={() => setOpen(!isOpen)}>
        <Date>{start}</Date>
        <TextContainer>
          <Name>{title}</Name>
          <p>
            {`@${location}`}
          </p>
        </TextContainer>
        <ExpandableIcon>
          {isOpen ? "-" : "+"}
        </ExpandableIcon>
      </div>
      {isOpen && (
        <OpenableContent>
          {!showSignup && (
            eventInfo
          )}
          {showSignup && (
            signupInfo
          )}
        </OpenableContent>
      )}
    </EventContainer>
  );
};

export default EventCard;
