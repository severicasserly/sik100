import React from "react";
import Image from "next/image";
import styled from "styled-components";
import { useTranslation } from "../i18n";

import theme from "../theme";

const eSettLogo = "/images/eSett_logo.png";
const ABBLogo = "/images/2000px-ABB_logo.png";

const IMG_SIZE = 300;

const MainSponsors = styled.div`
  display: flex;
  flex-flow: row wrap;
  justify-content: space-around;
  align-items: center;
  width: 100%;
  margin: ${theme.spacing.xs};

  @media screen and (max-width: ${theme.breakpoints.xxs}) {
    margin: ${theme.spacing.xs};
  }
`;

const SponsorTitle = styled.h2`
  color: ${theme.colors.yellow};
  text-transform: uppercase;
  text-align: center;
  padding: ${theme.spacing.sm};
`;

const Sponsors: React.FC = () => {
  const { t } = useTranslation();
  return (
    <>
      <SponsorTitle>{t("sponsored by")}</SponsorTitle>
      <MainSponsors>
        <Image
          src={ABBLogo}
          width={IMG_SIZE}
          height={IMG_SIZE}
          objectFit="scale-down"
        />
        <Image
          src={eSettLogo}
          width={IMG_SIZE}
          height={IMG_SIZE}
          objectFit="scale-down"
        />
      </MainSponsors>
    </>
  );
};

export default Sponsors;
