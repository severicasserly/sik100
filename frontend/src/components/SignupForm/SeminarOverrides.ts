const mapPackageToPrice = (packageType: string, userType: string) => {
  switch (userType) {
    case "Opiskelija / Student":
      switch (packageType) {
        case "Pelkkä seminaari / Seminar": return 20;
        case "Seminaari ja gaalaillallinen / Seminar & Gala Dinner": return 40;
        case "Kannatuspaketti (sis. 2 henkilöä seminaariin eturiviin ja gaalaillalliselle)": return 250;
        default: return 0;
      }
    case "SIK Alumni":
    case "muu / other":
      switch (packageType) {
        case "Pelkkä seminaari / Seminar": return 40;
        case "Seminaari ja gaalaillallinen / Seminar & Gala Dinner": return 75;
        case "Kannatuspaketti (sis. 2 henkilöä seminaariin eturiviin ja gaalaillalliselle)": return 250;
        default: return 0;
      }
    default: return 0;
  }
};

// sXeGhO3FR = Paketti
// Eub5f_WAS = Lukiolainen/opiskelija/alumni
// NqhAWLTdb = Vapaaehtoinen kannatusmaksu
// eslint-disable-next-line import/prefer-default-export
export const onChangeFunc = (setFormData: any, setPrice: any) => (data: any) => {
  if (data && data.formData) {
    const chosenPackage = mapPackageToPrice(data.formData.sXeGhO3FR, data.formData.Eub5f_WAS);
    const voluntaryPrice = data.formData.NqhAWLTdb || 0;
    setPrice(chosenPackage + voluntaryPrice);
    setFormData(data.formData);
  }
};
