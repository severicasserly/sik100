import React, { useState } from "react";
import Form, { ISubmitEvent } from "react-jsonschema-form";
import styled from "styled-components";
import { useTranslation } from "../../i18n";
import { EventSignupForm } from "../../models/events";
import Checkboxes from "./Checkboxes";
import RadioButtonWidget from "./RadioButtonWidget";
import SubmitButton from "./SubmitButton";
import { onChangeFunc } from "./SeminarOverrides";
import GDPRCheckbox from "./GDPRCheckbox";
import config from "../../config";

interface SignupFormProps {
  signup: EventSignupForm;
  onSubmit: (e: ISubmitEvent<any>) => any;
}

interface FormQuestion {
  id: string;
  type: string;
}

const customWidgets = {
  radio: RadioButtonWidget,
  checkboxes: Checkboxes,
};

const questionToUISchema = (question: FormQuestion) => {
  let obj = {};
  if (question.type === "checkbox") {
    obj = {
      "ui:widget": "checkboxes",
    };
  } else if (question.type === "radiobutton") {
    obj = {
      "ui:widget": "radio",
    };
  }
  return {
    [question.id]: obj,
  };
};

const buildUISchema = (questions: FormQuestion[]) => {
  const uiSchemaPropsArray = questions.map(questionToUISchema);
  let uiSchemaProps = {};
  uiSchemaPropsArray.forEach((uiSchemaProp) => {
    uiSchemaProps = {
      ...uiSchemaProps,
      ...uiSchemaProp,
    };
  });

  const uiSchema = {
    ...uiSchemaProps,
  };
  return uiSchema;
};

const StyledForm = styled(Form)`
  font-size: ${(p) => p.theme.fontSizes.normal};

  @media screen and (max-width: ${(p) => p.theme.breakpoints.sm}) {
    font-size: ${(p) => p.theme.fontSizes.small};
  }

  @media screen and (max-width: ${(p) => p.theme.breakpoints.xxs}) {
    font-size: ${(p) => p.theme.fontSizes.smaller};
  }

  fieldset {
    border: none;
    padding: 0;
    margin: 1em 0;
    display: block;
    margin-inline-start: 0;
    margin-inline-end: 0;
    padding-inline-start: 0;
    padding-inline-end: 0;

    .form-group {
      padding-bottom: 0.5em;
      display: flex;
      flex-direction: column;

      & > label {
        margin: 0.5em 0;
        font-weight: 600;
        display: block;
      }
    }

    input[type="text"],
    input[type="email"],
    input[type="number"] {
      display: block;
      height: 48px;
      flex: 1 0 auto;
      font-size: 1.5em;
      padding: 0 16px;
    }
  }

  & > div:last-child {
    text-align: center;
  }
`;

const Header = styled.h2`
  font-size: ${(p) => p.theme.fontSizes.h2};

  @media screen and (max-width: ${(p) => p.theme.breakpoints.sm}) {
    font-size: ${(p) => p.theme.fontSizes.normal};
  }
`;

const TotalPrice = styled.div`
  font-size: ${(p) => p.theme.fontSizes.normal};
  margin-top: 1em;

  @media screen and (max-width: ${(p) => p.theme.breakpoints.sm}) {
    font-size: ${(p) => p.theme.fontSizes.small};
  }

  @media screen and (max-width: ${(p) => p.theme.breakpoints.xxs}) {
    font-size: ${(p) => p.theme.fontSizes.smaller};
  }
`;

const SignupForm: React.FC<SignupFormProps> = ({ signup, onSubmit }) => {
  const { t } = useTranslation();
  const properties: any = {};
  signup.questions.forEach(({ id }: FormQuestion) => { properties[id] = signup.schema.properties[id]; });
  const formSchema = {
    ...signup.schema,
    properties,
  };

  const uiSchema = buildUISchema(signup.questions);

  // SIK100 seminar override
  const [controlledForm, setFormData] = useState();
  const [totalPrice, setTotalPrice] = useState(0);
  let formData;
  let onChange;
  if (signup.id === Number(config.seminaariSignupId)) {
    formData = controlledForm;
    onChange = onChangeFunc(setFormData, setTotalPrice);
  }

  if (!signup.isOpen) {
    return (
      <>
        <Header>
          {t("Ilmoittautuminen ei ole vielä auki!")}
        </Header>
        {t("Se aukeaa")}
        &nbsp;
        {new Date(signup.start_time).toLocaleString()}
      </>
    );
  }

  return (
    <>
      <Header>
        {t("Ilmoittautuminen")}
      </Header>
      <StyledForm
        schema={formSchema}
        uiSchema={uiSchema}
        onSubmit={onSubmit}
        onChange={onChange}
        formData={formData}
        widgets={customWidgets}
        idPrefix="rjsf"
      >
        <div>
          <GDPRCheckbox>
            {t("Ilmoittautumalla hyväksyn")}
            &nbsp;
            <a href="http://sik.ayy.fi/files/official/Tietosuojaseloste%20%E2%80%93%20Tapahtumien%20ilmoittautumisrekisteri.pdf">
              {t("tietosuojaselosteen")}
            </a>
            &nbsp;
            {t("ja tietojeni tallentamisen.")}
          </GDPRCheckbox>
        </div>

        {signup.id === Number(config.seminaariSignupId) && (
          <TotalPrice>
            {t("Kokonaishinta")}
            :&nbsp;
            <strong>
              {`${totalPrice} €`}
            </strong>
          </TotalPrice>
        )}

        <div>
          <SubmitButton>
            {t("Ilmoittaudu")}
          </SubmitButton>
        </div>
      </StyledForm>
    </>
  );
};

export default SignupForm;
