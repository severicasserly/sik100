import styled from "styled-components";

const SubmitButton = styled.button`
  background: ${(p) => p.theme.colors.goldGradient};
  margin: auto;
  cursor: pointer;
  padding: ${(p) => `${p.theme.spacing.sm} ${p.theme.spacing.md}`};
  text-transform: uppercase;
  font-size: ${(p) => p.theme.fontSizes.h2};
  font-weight: bold;
  color: ${(p) => p.theme.colors.darkBlue2};

  @media screen and (max-width: ${(p) => p.theme.breakpoints.sm}) {
    font-size: ${(p) => p.theme.fontSizes.normal};
    padding: ${(p) => `${p.theme.spacing.xs} ${p.theme.spacing.sm}`};
  }
`;

export default SubmitButton;
