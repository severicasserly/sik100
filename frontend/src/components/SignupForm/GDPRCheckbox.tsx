import React, { useState } from "react";
import styled from "styled-components";

const Container = styled.label`
  display: block;
  position: relative;
  padding-left: 2em;
  cursor: pointer;
  font-size: 1.5em; /* 24px */
  user-select: none;

  /* On mouse-over, add a grey background color */
  &:hover input ~ .custom-cbox {
    background-color: #d4d0c7; /* grey1 */
  }
`;

/* Hide the browser's default checkbox */
const HiddenDefaultElement = styled.input`
  position: absolute;
  opacity: 0;
  height: 0;
  width: 0;
`;

/* Create a custom checkbox */
const CustomCBoxElement = styled.span<{checked?: boolean}>`
  position: absolute;
  top: 0;
  left: 0;
  height: 1em;
  width: 1em;
  background-color: ${(props) => (props.checked ? "#57b2df" : "#efece4")}; /* blue1 or grey2 */
  border-radius: 4px;

  &:focus &::before {
    transition: box-shadow 150ms ease;
    content: '';
    display: block;
    position: absolute;
    top: -4px;
    bottom: -4px;
    left: -4px;
    right: -4px;
    border-radius: 6px;
    border: 2px solid color(blue);
  }
`;

const Checkmark = styled.div`
  position: absolute;
  left: 0.35em;
  top: calc(0.25em - 2px);
  width: 0.25em;
  height: 0.5em;
  border: solid #fff;
  border-width: 0 0.125em 0.125em 0;
  transform: rotate(45deg);
`;

type CheckboxProps = Omit<
  React.InputHTMLAttributes<HTMLInputElement>,
  "type" | "required" | "onChange"
>;

const GDPRCheckbox: React.FC<CheckboxProps> = ({ children, ...props }) => {
  const [checkState, setCheck] = useState(false);
  return (
    <Container>
      {children}
      <HiddenDefaultElement
        {...props}
        type="checkbox"
        required
        tabIndex={-1}
        onChange={(e) => setCheck(e.target.checked)}
      />
      <CustomCBoxElement tabIndex={0} checked={checkState} className="custom-cbox">
        {checkState && (<Checkmark />)}
      </CustomCBoxElement>
    </Container>
  );
};

export default GDPRCheckbox;
