import React from "react";
import styled from "styled-components";

const Container = styled.label`
  display: block;
  position: relative;
  padding-left: 2em;
  cursor: pointer;
  font-size: 1.5em;
  user-select: none;

  /* On mouse-over, add a grey background color */
  &:hover input ~ .custom-radio {
    background-color: #d4d0c7; /* grey1 */
  }
`;

const HiddenDefaultElement = styled.input`
  position: absolute;
  opacity: 0;
  height: 0;
  width: 0;
`;

const CustomRadioElement = styled.span<{checked?: boolean}>`
  position: absolute;
  top: 0;
  left: 0;
  height: 1em;
  width: 1em;
  background-color: ${(props) => (props.checked ? "#57b2df" : "#efece4")}; /* blue1 or grey2 */
  border-radius: 50%;
`;

const Indicator = styled.div`
  position: absolute;
  top: 0.25em;
  left: 0.25em;
  width: 0.5em;
  height: 0.5em;
  border-radius: 50%;
  background: white;
`;

type RadioButtonProps = Omit<
  React.InputHTMLAttributes<HTMLInputElement>,
  "type"
>;

const RadioButton: React.FC<RadioButtonProps> = ({
  checked,
  children,
  ...props
}) => (
  <Container>
    {children}
    <HiddenDefaultElement
      type="radio"
      checked={checked}
      {...props}
      tabIndex={-1}
    />
    <CustomRadioElement tabIndex={0} checked={checked} className="custom-radio">
      {checked && (<Indicator />)}
    </CustomRadioElement>
  </Container>
);

export default RadioButton;
