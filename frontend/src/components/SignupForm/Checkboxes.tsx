import React from "react";
import { WidgetProps } from "react-jsonschema-form";
import styled from "styled-components";
import Checkbox from "./Checkbox";

/* eslint-disable no-shadow */
/* eslint-disable react/no-array-index-key */
/* eslint-disable jsx-a11y/label-has-associated-control */

// See https://github.com/rjsf-team/react-jsonschema-form/blob/master/packages/core/src/components/widgets/CheckboxesWidget.js

function selectValue(value: any, selected: any, all: any[]) {
  const at = all.indexOf(value);
  const updated = selected.slice(0, at).concat(value, selected.slice(at));
  // As inserting values at predefined index positions doesn't work with empty
  // arrays, we need to reorder the updated selection to match the initial order
  return updated.sort((a: any, b: any) => all.indexOf(a) > all.indexOf(b));
}

function deselectValue(value: any, selected: any) {
  return selected.filter((v: any) => v !== value);
}

type CheckboxesProps = Omit<WidgetProps, "options"> & {
  options: any;
};

const CheckboxContainer = styled.div`
  margin-bottom: 0.5em;
`;

const Checkboxes: React.FC<CheckboxesProps> = ({
  id, disabled, options, value, autofocus, readonly, onChange,
}) => {
  const { enumOptions, enumDisabled, inline } = options;
  return (
    <div className="checkboxes" id={id}>
      {enumOptions.map((option: any, index: number) => {
        const checked = value.indexOf(option.value) !== -1;
        const itemDisabled =
      enumDisabled && enumDisabled.indexOf(option.value) !== -1;
        const disabledCls =
      disabled || itemDisabled || readonly ? "disabled" : "";
        const checkbox = (
          <Checkbox
            id={`${id}_${index}`}
            checked={checked}
            disabled={disabled || itemDisabled || readonly}
            autoFocus={autofocus && index === 0}
            onChange={(event) => {
              const all = enumOptions.map(({ value }: any) => value);
              if (event.target.checked) {
                onChange(selectValue(option.value, value, all));
              } else {
                onChange(deselectValue(option.value, value));
              }
            }}
          >
            {option.label}
          </Checkbox>
        );
        return inline ? (
          <label key={index} className={`checkbox-inline ${disabledCls}`}>
            {checkbox}
          </label>
        ) : (
          <CheckboxContainer key={index} className={disabledCls}>
            {checkbox}
          </CheckboxContainer>
        );
      })}
    </div>
  );
};

Checkboxes.defaultProps = {
  autofocus: false,
  options: {
    inline: false,
  },
};

export default Checkboxes;
