import React from "react";
import { WidgetProps } from "react-jsonschema-form";
import styled from "styled-components";
import RadioButton from "./RadioButton";

/* eslint-disable no-shadow */
/* eslint-disable react/no-array-index-key */
/* eslint-disable jsx-a11y/label-has-associated-control */

type RadioButtonWidgetProps = Omit<WidgetProps, "options"> & {
  options: any;
};

const RadioButtonContainer = styled.div`
  margin-bottom: 0.5em;
`;

const RadioButtonWidget: React.FC<RadioButtonWidgetProps> = (props) => {
  const {
    options,
    value,
    required,
    disabled,
    readonly,
    autofocus,
    onBlur,
    onFocus,
    onChange,
    id,
  } = props;
  // Generating a unique field name to identify this set of radio buttons
  const name = Math.random().toString();
  const { enumOptions, enumDisabled, inline } = options;
  // checked={checked} has been moved above name={name}, As mentioned in #349;
  // this is a temporary fix for radio button rendering bug in React, facebook/react#7630.
  return (
    <div className="field-radio-group" id={id}>
      {enumOptions.map((option: any, i: number) => {
        const checked = option.value === value;
        const itemDisabled =
          enumDisabled && enumDisabled.indexOf(option.value) !== -1;
        const disabledCls =
          disabled || itemDisabled || readonly ? "disabled" : "";
        const radio = (
          <RadioButton
            checked={checked}
            name={name}
            required={required}
            value={option.value}
            disabled={disabled || itemDisabled || readonly}
            autoFocus={autofocus && i === 0}
            onChange={() => onChange(option.value)}
            onBlur={onBlur && ((event) => onBlur(id, event.target.value))}
            onFocus={onFocus && ((event) => onFocus(id, event.target.value))}
          >
            {option.label}
          </RadioButton>
        );

        return inline ? (
          <label key={i} className={`radio-inline ${disabledCls}`}>
            {radio}
          </label>
        ) : (
          <RadioButtonContainer key={i} className={disabledCls}>
            {radio}
          </RadioButtonContainer>
        );
      })}
    </div>
  );
};

RadioButtonWidget.defaultProps = {
  autofocus: false,
};

export default RadioButtonWidget;
