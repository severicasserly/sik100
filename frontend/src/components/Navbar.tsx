import React, { useState } from "react";
import Link from "next/link";
import i18n, { useTranslation } from "../i18n";
import { useRouter } from "next/router";

import styled from "styled-components";
import GoldHR from "./GoldHR";
import theme from "../theme";

const Container = styled.div`
    background: linear-gradient(0deg, ${theme.colors.darkBlue2} 50%, ${theme.colors.blue5});
    display: flex;
    flex-direction: row nowrap;
    justify-content: center;
    align-items: center;
    padding: ${theme.spacing.md} ${theme.spacing.sm};
    text-decoration: uppercase;
    letter-spacing: 1px;
    font-weight: 700;

    @media screen and (max-width: ${theme.breakpoints.sm}) {
      font-size: 12px;
      padding: ${theme.spacing.sm};
      justify-content: flex-end;
    }
`;

const MobileHamburger = styled.svg`
  min-height: 32px;
  min-width: 32px;
  max-height: 32px;
  max-width: 32px;
  fill: ${theme.colors.white};
  cursor: pointer;

  @media screen and (min-width: ${theme.breakpoints.sm}) {
    display: none;
  }
`;

const MobileLinkContainer = styled.div`
  display: flex;
  flex-direction: column;
  background: ${theme.colors.darkBlue2};
  padding: ${theme.spacing.md} 0;

  @media screen and (min-width: ${theme.breakpoints.sm}) {
    display: none;
  }
`;

const Sticky = styled.div`
  position: sticky;
  top: 0;
  z-index: 1000;
`;

const MobileHide = styled.div`
  display: none;

  @media screen and (min-width: ${theme.breakpoints.sm}) {
    display: block;
  }
`;

const CustomNavLink = styled.a<{active?: boolean}>`
  text-transform: uppercase;
  text-decoration: none;
  border-bottom: 1px solid transparent;

  color: ${({ active }) => (active ? theme.colors.primaryGold : theme.colors.white)};
  margin: 0 ${theme.spacing.xs};

  @media screen and (max-width: ${theme.breakpoints.xs}) {
    margin: ${theme.spacing.xs} ${theme.spacing.md};
    text-transform: uppercase;
    text-decoration: none;
    text-align: center;
  }

  &:focus,
  &:visited,
  &:link,
  &:active {
    text-decoration: none;
  }

  &:hover {
    color: ${theme.colors.primaryGold};
    transition:color .5s ease-out;
    border-bottom: 1px solid white;
  }
`;

const Navbar: React.FC = () => {
  const [isOpen, setOpen] = useState(false);

  const router = useRouter();
  const onClick = (): void => setOpen(false);

  const { changeLanguage, language } = i18n.i18n;
  const { t } = useTranslation();

  const Links: React.FC = () => (
    <>
      <Link href="/">
        <CustomNavLink active={router.pathname === "/"} onClick={onClick}>
          {t("frontpage")}
        </CustomNavLink>
      </Link>
      <Link href="/events">
        <CustomNavLink active={router.pathname === "/events"} onClick={onClick}>
          {t("events")}
        </CustomNavLink>
      </Link>
      <Link href="/sponsors">
        <CustomNavLink active={router.pathname === "/sponsors"} onClick={onClick}>
          {t("sponsorspage")}
        </CustomNavLink>
      </Link>
      <Link href="/project">
        <CustomNavLink active={router.pathname === "/project"} onClick={onClick}>
          {t("projectpage")}
        </CustomNavLink>
      </Link>
      <Link href="/products">
        <CustomNavLink active={router.pathname === "/products"} onClick={onClick}>
          {t("productpage")}
        </CustomNavLink>
      </Link>
      <Link href="/contacts">
        <CustomNavLink active={router.pathname === "/contacts"} onClick={onClick}>
          {t("contactspage")}
        </CustomNavLink>
      </Link>
      <CustomNavLink onClick={() => changeLanguage(language === "fi" ? "en" : "fi")}>
        {t("lngButton")}
      </CustomNavLink>
    </>
  );

  return (
    <Sticky>
      <Container>
        <MobileHide>
          <Links />
        </MobileHide>
        <MobileHamburger
          onClick={(): void => { setOpen(!isOpen); }}
          role="img"
          viewBox="0 0 32 32"
          xmlns="http://www.w3.org/2000/svg"
        >
          <title>Menu</title>
          {/* eslint-disable-next-line */}
          <path d="M4,10h24c1.104,0,2-0.896,2-2s-0.896-2-2-2H4C2.896,6,2,6.896,2,8S2.896,10,4,10z M28,14H4c-1.104,0-2,0.896-2,2  s0.896,2,2,2h24c1.104,0,2-0.896,2-2S29.104,14,28,14z M28,22H4c-1.104,0-2,0.896-2,2s0.896,2,2,2h24c1.104,0,2-0.896,2-2  S29.104,22,28,22z" />
        </MobileHamburger>
      </Container>
      <GoldHR />
      {isOpen && (
        <MobileLinkContainer>
          <Links />
        </MobileLinkContainer>
      )}
    </Sticky>
  );
};

export default Navbar;
