import React from "react";
import { useTranslation } from "../i18n";

import { Event } from "../models/events";
import EventCard from "./EventCard";
import EmptyEvent from "./EventCard/EmptyEvent";

type UpcomingEventsProps = {
  events: Event[];
  focusOn?: number;
}

const UpcomingEvents: React.FC<UpcomingEventsProps> = ({
  events, focusOn,
}) => {
  const { i18n } = useTranslation();

  // TODO: For empty array, renders "loading" state. Keeping it since we have no empty events state in styles.
  if (!events?.length) {
    return (
      <>
        <EmptyEvent key="empty1" />
        <EmptyEvent key="empty2" />
        <EmptyEvent key="empty3" />
      </>
    );
  }

  return (
    <>
      {
      events.map((e) => {
        const startDate = new Date(e.start_time);
        const dateString = startDate.getFullYear() >= 2021
          ? startDate.toLocaleDateString("en-US", { month: "numeric", year: "2-digit" })
          : startDate.toLocaleDateString("fi-FI", { day: "numeric", month: "numeric" });
        return (
          <EventCard
            key={e.id}
            focused={focusOn === e.id}
            title={i18n.language === "fi" ? e.title_fi : e.title_en}
            start={dateString}
            location={i18n.language === "fi" ? e.location_fi : e.location_en}
            content={i18n.language === "fi" ? e.content_fi : e.content_en}
            signupForm={e.signupForm.length > 0 ? e.signupForm[0] : undefined}
            image={e.image}
          />
        );
      })
      }
    </>
  );
};

export default UpcomingEvents;
