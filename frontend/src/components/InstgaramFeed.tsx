import React from "react";
import styled from "styled-components";
import { useTranslation } from "../i18n";
import { InstagramObject } from "../models/instagram";

const Instagram = styled.div`
  h2 {
    color: ${(p) => p.theme.colors.yellow};
    text-transform: uppercase;
    text-align: center;
    margin-top: ${(p) => p.theme.spacing.lg};
  }

  & > div {
    display: flex;
    flex-direction: row;
    flex-wrap: wrap;
    justify-content: space-around;
  }

  figure {
    color: ${(p) => p.theme.colors.white};
    margin: ${(p) => p.theme.spacing.sm};
    width: min-content;
    display: inline-table;

    @media screen and (max-width: ${(p) => p.theme.breakpoints.xxs}) {
      margin: 0;
    }

    img,
    video {
      max-width: 250px;
      max-height: 250px;
      object-fit: scale-down;

      @media screen and (max-width: ${(p) => p.theme.breakpoints.xxxs}) {
        max-width: 200px;
        max-height: 200px;
      }
    }

    @media screen and (max-width: ${(p) => p.theme.breakpoints.xxxs}) {
      height: 200px;
      width: 200px;
    }
  }
`;

const ImageBackground = styled.div`
  height: 250px;
  width: 250px;

  background-color: black;
  display: flex;
  justify-content: center;
  align-items: center;
`;

const ImgCaption = styled.figcaption`
  color: ${(p) => p.theme.colors.white};
  font-size: 14px;
  text-decoration: none;
  overflow: hidden;
  max-width: 250px;
  margin: 1em 0;

  /*
  custom three dots for multiline component
  hide text if it more than N lines
  use this value to count block height
  */

  position: relative;

  /* max-height = line-height (1.5) * lines max number (3) */
  line-height: 1.5;
  max-height: 4.5em;
  transition: max-height 0.5s cubic-bezier(0, 1, 0, 1);

  /* fix problem when last visible word doesn't adjoin right side  */
  text-align: justify;

  /* place for "..." */
  margin-right: -1em;
  padding-right: 1em;

  /* create the ... */
  &::before {
    content: "...";
    position: absolute;
    right: 0;
    bottom: 0;
  }

  /* hide ... if we have text, which is less than or equal to max lines */
  &::after {
    content: "";
    position: absolute;
    right: 0;
    width: 1em;
    height: 1em;
    margin-top: 0.2em;
    background: ${(p) => p.theme.colors.blue6};
  }

  &:hover {
    /* Just something bigger than the text will ever be. Transition does not work for fit-content or anything not exactly a number */
    max-height: 2000px;
    transition: max-height 1s ease-in-out;

    /* hide the ... */
    &::before {
      content: "";
    }

    &::after {
      background: transparent;
    }
  }
`;

interface InstagramFeedProps {
  feed: InstagramObject[];
}

const InstagramFeed: React.FC<InstagramFeedProps> = ({ feed }) => {
  const { t } = useTranslation();
  return (
    <Instagram>
      <h2>{t("Seuraa sosiaalisessa mediassa")}</h2>
      <div>
        {feed.map((instagramObject) => (
          <figure key={instagramObject.id}>
            <a
              href={instagramObject.link}
              aria-label="Instagram"
            >
              <ImageBackground>
                {instagramObject.type === "VIDEO"
                  ? (
                // eslint-disable-next-line jsx-a11y/media-has-caption
                    <video controls>
                      <source src={instagramObject.img} type="video/mp4" />
                    </video>
                  )
                  : (
                    <img
                      src={instagramObject.img}
                      alt={`Instagram-${instagramObject.id}`}
                      width={250}
                      height={250}
                      loading="lazy"
                    />
                  )}
              </ImageBackground>
            </a>
            <ImgCaption>{instagramObject.text}</ImgCaption>
          </figure>
        ))}
      </div>
    </Instagram>
  );
};

export default InstagramFeed;
