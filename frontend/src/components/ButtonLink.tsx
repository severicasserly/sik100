import React from "react";
import Button, { ButtonProps } from "./Button";
import styled from "styled-components";

type ButtonLinkProps = ButtonProps & {
  link: string;
}

const A = styled.a`
  text-decoration: none;

  &:focus,
  &:visited,
  &:link,
  &:active {
    text-decoration: none;
  }
`;

const ButtonLink: React.FC<ButtonLinkProps> = ({ link, ...props }) => (
  <A href={link} target="_blank">
    <Button {...props} />
  </A>
);

export default ButtonLink;
