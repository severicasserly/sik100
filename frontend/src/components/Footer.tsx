import React from "react";
import styled from "styled-components";
import Logo from "./Logo";
import theme from "../theme";

const Container = styled.footer`
  color: ${theme.colors.white};
  display: flex;
  align-items: center;
  min-height: 200px;
  flex: 0 1;
  background: rgba(0, 0, 0, 0.2);
  padding: 0 ${theme.spacing.xlg};
  @media screen and (max-width: ${theme.breakpoints.xs}) {
    padding: 0 ${theme.spacing.lg};
  }

  @media screen and (max-width: ${theme.breakpoints.xxxs}) {
    padding: 0 ${theme.spacing.md};
  }

  img {
    max-width: 100px;
    flex: 1 0;
    padding-right: 20px;
  }

  p {
    font-size: ${theme.fontSizes.normal};

    @media screen and (max-width: ${theme.breakpoints.sm}) {
      font-size: ${theme.fontSizes.small};
    }
  }
`;

const Footer: React.FC = () => (
  <Container>
    <Logo />
    <div>
      <p>© Aalto-yliopiston Sähköinsinöörikilta ry</p>
      <p>Webmaster Aarni Halinen (aarni@sik100.fi)</p>
      <p>Design by Jutta Kalli</p>
    </div>
  </Container>
);

export default Footer;
