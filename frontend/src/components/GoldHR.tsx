import styled from "styled-components";

const GoldHR = styled.hr`
  margin-block-start: 0;
  margin-block-end: 0;
  border: 0;
  background: ${(p) => p.theme.colors.goldGradient};
  height: 8px;
`;

export default GoldHR;
