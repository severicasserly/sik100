import React, { ButtonHTMLAttributes } from "react";
import styled from "styled-components";

export type ButtonProps = ButtonHTMLAttributes<HTMLButtonElement> & {
}

const GradientBorder = styled.button`
  background: ${(p) => p.theme.colors.goldGradient};
  padding: 1px;
  cursor: pointer;

  & > div {
    background: ${(p) => p.theme.colors.darkBlue2};
    padding: ${(p) => `${p.theme.spacing.sm} ${p.theme.spacing.md}`};
    text-transform: uppercase;
    text-align: center;
    color: ${(p) => p.theme.colors.white};

    @media screen and (max-width: ${(p) => p.theme.breakpoints.sm}) {
      font-size: ${(p) => p.theme.fontSizes.smaller};
      padding: ${(p) => `${p.theme.spacing.xs} ${p.theme.spacing.sm}`};
    }
  }
`;

const Button: React.FC<ButtonProps> = ({ children, ...props }) => (
  <GradientBorder {...props}>
    <div>
      {children}
    </div>
  </GradientBorder>
);

export default Button;
