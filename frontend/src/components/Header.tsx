import React from "react";
import Navbar from "./Navbar";
import Banner, { BannerProps } from "./Banner";

const Header: React.FC<BannerProps> = (props) => (
  <>
    <Navbar />
    <header>
      <Banner {...props} />
    </header>
  </>
);

export default Header;
