import React from "react";
import { NextPage, GetServerSideProps } from "next";
import { Event } from "../models/events";
import { InstagramObject } from "../models/instagram";
import useFetchEvents, { fetchData as fetchEvents } from "../hooks/useFetchEvents";
import useFetchInstagram, { fetchData as fetchInstagram } from "../hooks/useFetchInstagram";
import LandingView from "../views/LandingView";

interface LandingInitialProps {
  initialEvents: Event[];
  initialInstagramFeed: InstagramObject[];
}

const LandingPage: NextPage<LandingInitialProps> = ({ initialEvents, initialInstagramFeed }) => {
  const { events } = useFetchEvents(initialEvents);
  const { instagramFeed } = useFetchInstagram(initialInstagramFeed);
  return (
    <LandingView events={events ?? []} instagramFeed={instagramFeed ?? []} />
  );
};

export const getServerSideProps: GetServerSideProps<LandingInitialProps> = async () => {
  const initialEvents = await fetchEvents();
  const initialInstagramFeed = await fetchInstagram();
  return {
    props: {
      initialEvents,
      initialInstagramFeed,
    },
  };
};

export default LandingPage;
