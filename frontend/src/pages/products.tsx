import React from "react";
import { NextPage, GetStaticProps } from "next";
import ProductsView from "../views/ProductsView";

const ProductsPage: NextPage = () => (
  <ProductsView />
);

export const getStaticProps: GetStaticProps = async () => ({
  props: {},
});

export default ProductsPage;
