import React from "react";
import { AppProps } from "next/app";
import Head from "next/head";
import styled, { createGlobalStyle, ThemeProvider } from "styled-components";
import { ToastContainer } from "react-toastify";
import i18n from "../i18n";
import { theme } from "../theme";

import "react-toastify/dist/ReactToastify.css";

const GlobalCommonStyles = createGlobalStyle`
  body {
    margin: 0;
    line-height: 1.5;
    -webkit-font-smoothing: antialiased;
    -moz-osx-font-smoothing: grayscale;
  }

  * {
    font-family: Montserrat, sans-serif;
  }

  main {
    display: flex;
    flex-direction: column;
    align-items: center;
    flex: 1 0 auto;
    color: ${theme.colors.white};

    section {
      box-sizing: border-box;
      margin: 0 ${theme.spacing.lg};
      max-width: ${theme.maxContentWidth};

      @media screen and (max-width: ${theme.breakpoints.xxxs}) {
        margin: 0 ${theme.spacing.md};
      }
    }
  }
`;

const AppContainer = styled.div`
  display: flex;
  flex-direction: column;
  flex: 1 0 auto;
  align-items: stretch;
  min-height: 100vh;
  background-color: ${theme.colors.darkBlue2};
`;

const SIK100App = ({ Component, pageProps }: AppProps) => (
  <>
    <Head>
      <meta httpEquiv="Content-Type" content="text/html; charset=utf-8" />
      <link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,700,800&display=swap" rel="stylesheet" />
      <link rel="icon" href="/favicon.ico" />
      <meta httpEquiv="X-UA-Compatible" content="IE=edge" />
      <meta name="viewport" content="width=device-width, initial-scale=1" />
      <meta name="description" content="SIK turns 100 years old!" />
      <meta name="keywords" content="SIK100" />
      <title>SIK100</title>

      <link rel="manifest" href="/manifest.json" />
      <link rel="apple-touch-icon" href="/logo192.png" />
      <meta name="theme-color" content={theme.colors.darkBlue2} />
    </Head>
    <GlobalCommonStyles />
    <ThemeProvider theme={theme}>
      <AppContainer>
        {/* <Countdown revealDate={config.appRevealDate ? new Date(config.appRevealDate) : new Date()}> */}
        <Component {...pageProps} />
        {/* </Countdown> */}
        <ToastContainer />
      </AppContainer>
    </ThemeProvider>
  </>
);

export default i18n.appWithTranslation(SIK100App);
