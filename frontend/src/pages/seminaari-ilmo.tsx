import React from "react";
import { NextPage, GetServerSideProps } from "next";
import config from "../config";
import { Event } from "../models/events";
import useFetchEvents, { fetchData } from "../hooks/useFetchEvents";
import EventView from "../views/EventView";
import ErrorView from "../views/ErrorView";

interface EventInitialProps {
  initialEvents: Event[];
}

const SeminaariIlmoPage: NextPage<EventInitialProps> = ({ initialEvents }) => {
  const { events, error } = useFetchEvents(initialEvents);
  if (error) {
    return (
      <ErrorView statusCode={500} />
    );
  }

  return (
    <EventView events={events ?? []} focusOn={Number(config.seminaariEventId)} />
  );
};

export const getServerSideProps: GetServerSideProps<EventInitialProps> = async () => {
  const initialEvents = await fetchData();
  return {
    props: {
      initialEvents,
    },
  };
};

export default SeminaariIlmoPage;
