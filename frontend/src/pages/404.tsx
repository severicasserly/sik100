import React from "react";
import { GetStaticProps } from "next";
import { useTranslation } from "../i18n";
import ErrorView from "../views/ErrorView";
import { isBrowser } from "../utils";

const NotFoundPage: React.FC = () => {
  const { t } = useTranslation();
  return (
    <ErrorView statusCode={404}>
      <h3>
        {t("Hakemaasi sivua")}
        &nbsp;
        {isBrowser() && (
          <code>{window.location.pathname}</code>
        )}
        &nbsp;
        {t("ei löydy")}
      </h3>
    </ErrorView>
  );
};

export const getStaticProps: GetStaticProps = async () => ({
  props: {},
});

export default NotFoundPage;
