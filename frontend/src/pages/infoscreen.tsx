import React from "react";
import { NextPage, GetServerSideProps } from "next";
import { Event } from "../models/events";
import useFetchEvents, { fetchData } from "../hooks/useFetchEvents";
import UpcomingEvents from "../components/UpcomingEvents";
import ErrorView from "../views/ErrorView";

const EVENT_LIMIT = 3;

interface InfoscreenInitialProps {
  initialEvents: Event[];
}

const InfoscreenPage: NextPage<InfoscreenInitialProps> = ({ initialEvents }) => {
  const { events, error } = useFetchEvents(initialEvents);
  if (error) {
    return (
      <ErrorView statusCode={500} />
    );
  }
  return (
    <UpcomingEvents events={events ?? []} />
  );
};

export const getServerSideProps: GetServerSideProps<InfoscreenInitialProps> = async () => {
  const events = await fetchData();
  return {
    props: {
      initialEvents: events.slice(0, EVENT_LIMIT),
    },
  };
};

export default InfoscreenPage;
