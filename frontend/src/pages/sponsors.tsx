import React from "react";
import { NextPage, GetStaticProps } from "next";
import SponsorsView from "../views/SponsorsView";

const SponsorsPage: NextPage = () => (
  <SponsorsView />
);

export const getStaticProps: GetStaticProps = async () => ({
  props: {},
});

export default SponsorsPage;
