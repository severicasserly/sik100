const sitemap = require('nextjs-sitemap-generator');
const baseUrl = 'https://sik100.fi';

console.log('process.env:', process.env);

sitemap({
  baseUrl,
  pagesDirectory: __dirname + "/../src/pages",
  targetDirectory : __dirname + "/../public",
  ignoredPaths: ['locales', ],
  ignoredExtensions: [
        'png',
        'jpg',
        'ico',
        'svg',
        'xml'
  ],
});

console.log(`✅ sitemap.xml generated!`);